package com.example.miprimeraplicacion.model

class ResultadoCovidClass(val fiebre: String, val tos: String, val edad: String) {

    fun obtenerResultados():salud {
        if (tos == tosResult.NO.toString() && fiebre == fiebreResult.NO.toString() && edad == edadResult.MENOS_65.toString()) {
            val saludPac = salud.SANO

            return saludPac
        } else if ((fiebre == fiebreResult.SI.toString() && tos == tosResult.NO.toString() && edad == edadResult.MENOS_65.toString())) {
            val saludPac = salud.CUIDATE
            return saludPac

        } else if (fiebre == fiebreResult.SI.toString() && tos == tosResult.NO.toString() && edad == edadResult.MAYOR_IGUAL_65.toString()) {
            val saludPac = salud.CONTROLATE

            return saludPac
        } else if (fiebre == fiebreResult.SI.toString() && tos == tosResult.SI.toString() && edad == edadResult.MAYOR_IGUAL_65.toString()) {
            val saludPac = salud.CONTROLATE
            return saludPac
        } else {
            val saludPac = salud.CUIDATE
            return saludPac
        }

    }
}

internal enum class tosResult{
    SI,
    NO
}
enum class fiebreResult{
    SI,
    NO
}
enum class edadResult{
    MENOS_65,
    MAYOR_IGUAL_65
}
enum class salud{
    SANO,
    CUIDATE,
    CONTROLATE
}
