package com.example.miprimeraplicacion.ui.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.miprimeraplicacion.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val iniciarSesion = findViewById<Button>(R.id.btnLogin)

        iniciarSesion.setOnClickListener{

            val usuario = (findViewById<EditText>(R.id.etUser)).text.toString()
            val pass = (findViewById<EditText>(R.id.etPass)).text.toString()

            if (usuario == "Ailin" && pass == "123"){
                sendMessage()
            }
            else if(usuario != "Ailin" && pass=="123"){
                val intent = Intent(this, PopUpActivity::class.java)
                val error = "El usuario '$usuario' ingresado, no es válido"
                intent.putExtra("Error", error)
                startActivity(intent)
            }
            else if(usuario == "Ailin" && pass != "123")
            {
                val intent = Intent(this, PopUpActivity::class.java)
                val error = "El pass '$pass' ingresado, no es válido"
                intent.putExtra("Error", error)
                startActivity(intent)
            }
            else{
                val intent = Intent(this, PopUpActivity::class.java)
                val error = "El usuario '$usuario' y el pass '$pass' ingresado, no son válidos"
                intent.putExtra("Error", error)
                startActivity(intent)
            }


        }
    }
    private fun sendMessage(){
        val intent = Intent(this, TesteoCovid::class.java)
        startActivity(intent)
    }
}






