package com.example.miprimeraplicacion.ui.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import com.example.miprimeraplicacion.R
import com.example.miprimeraplicacion.model.edadResult
import com.example.miprimeraplicacion.model.fiebreResult
import com.example.miprimeraplicacion.model.tosResult

class TesteoCovid : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_testeo_covid)

        val btnAutotesteo = findViewById<Button>(R.id.btnAutotesteo)
        btnAutotesteo.setOnClickListener{
            val intent = Intent(this, ResultadoTesteo::class.java)
            val array = compararValores()
            intent.putExtra("Valor1", array[0])
            intent.putExtra("Valor2", array[1])
            intent.putExtra("Valor3", array[2])

            startActivity(intent)
        }
    }
    fun compararValores(): MutableList<String> {
        val radioGFiebre = findViewById<RadioGroup>(R.id.rgFiebre)
        val rbFiebreNoRef = findViewById<RadioButton>(R.id.rbFiebreNo)


        val radioGFTos = findViewById<RadioGroup>(R.id.rgTos)
        val rbTosNoRef = findViewById<RadioButton>(R.id.rbTosNo)


        val textEdad: Int? = findViewById<EditText>(R.id.txtEdad).text.toString().toIntOrNull()


        val idRadioButtonSelFib = radioGFiebre.checkedRadioButtonId
        val idRadioButtonSelTos = radioGFTos.checkedRadioButtonId

        val arrayValores = mutableListOf<String>()
        if(idRadioButtonSelFib == rbFiebreNoRef.id)
        {
            val resultFiebre:String = fiebreResult.NO.toString()
            arrayValores.add(resultFiebre)

        }
        else{

            val resultFiebre = fiebreResult.SI.toString()
            arrayValores.add(resultFiebre)
        }

        if(idRadioButtonSelTos == rbTosNoRef.id)
        {
            val resultTos = tosResult.NO.toString()
            arrayValores.add(resultTos)
        }
        else{
            val resultTos = tosResult.SI.toString()
            arrayValores.add(resultTos)
        }
        if (textEdad != null) {
            if(textEdad >= 65) {
                val resultEdad = edadResult.MAYOR_IGUAL_65.toString()
                arrayValores.add(resultEdad)
            }
            else{
                val resultEdad = edadResult.MENOS_65.toString()
                arrayValores.add(resultEdad)
            }
        }
        else{
            val resultEdad = "NULL"
            arrayValores.add(resultEdad)
        }

        return arrayValores

    }


}
