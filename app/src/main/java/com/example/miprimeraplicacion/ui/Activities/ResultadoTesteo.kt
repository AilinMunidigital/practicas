package com.example.miprimeraplicacion.ui.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.miprimeraplicacion.Fragments.HealthyFragment
import com.example.miprimeraplicacion.Fragments.PayAttentionHealthFragment
import com.example.miprimeraplicacion.Fragments.RequierControlFragment
import com.example.miprimeraplicacion.R
import com.example.miprimeraplicacion.model.ResultadoCovidClass
import com.example.miprimeraplicacion.model.salud

class ResultadoTesteo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado_testeo)

        val objetoIntent: Intent = intent
        val valor1 = objetoIntent.getStringExtra("Valor1")
        val valor2 = objetoIntent.getStringExtra("Valor2")
        val valor3 = objetoIntent.getStringExtra("Valor3")

        if (valor1 != null && valor2 != null && valor3 != null) {
            obtenerFragment(valor1,valor2,valor3)
        }
        else{
            Toast.makeText(this,"Faltan valores por ingresar", Toast.LENGTH_SHORT).show()
        }
    }
    private fun obtenerFragment(valor1: String, valor2: String, valor3: String){
        val healthy = HealthyFragment()
        val control = RequierControlFragment()
        val danger = PayAttentionHealthFragment()


        if (valor3 != "NULL"){
            val resultadosCovid = ResultadoCovidClass(valor1, valor2, valor3)
            val stringAviso = resultadosCovid.obtenerResultados()

            when (stringAviso) {
                salud.SANO -> {
                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.frColor,healthy)
                        addToBackStack(null)
                        commit()
                    }
                }
                salud.CUIDATE -> {
                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.frColor,control)
                        addToBackStack(null)
                        commit()
                    }
                }
                else -> {
                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.frColor,danger)
                        addToBackStack(null)
                        commit()
                    }
                }
            }
        }
        else{
            Toast.makeText(this, "No ingresó edad", Toast.LENGTH_SHORT).show()
        }

    }





}






