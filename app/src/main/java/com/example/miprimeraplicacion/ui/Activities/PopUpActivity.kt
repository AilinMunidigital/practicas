package com.example.miprimeraplicacion.ui.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.miprimeraplicacion.R

class PopUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pop_up)

        val objetoIntent: Intent = intent
        val errorLogin = objetoIntent.getStringExtra("Error")
        val txtError = findViewById<TextView>(R.id.tvError)
        txtError.text = errorLogin
    }
}